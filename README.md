**Realistic Juggling HD**
=========================

The weight of the world is in your hands!
	
In this short game, the player must juggle multiple Earths until the day is
over. If the player drops just one planet, the player must try again another
day. A special message appears for those who are worthy.

Controls
--------
- A to hold ball in left hand
- A + S to throw ball in left hand to the right
- L to hold ball in right hand
- L + K to throw ball in right hand to the left
- R to reset
- Esc to quit
