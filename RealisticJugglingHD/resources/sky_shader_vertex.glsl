#version 330 core
uniform mat4 P;
uniform mat4 V;
uniform mat4 M;
uniform vec3 sunPosition;

layout(location = 0) in vec3 vertPos;
layout(location = 1) in vec3 vertNor;
layout(location = 2) in vec2 vertTex;

out vec3 vertex_pos;
out vec3 vertex_normal;
out vec2 vertex_tex;

out vec3 sun_pos;

void main() {
    vec4 tpos =  M * vec4(vertPos, 1.0);
    vertex_pos = tpos.xyz;
    vertex_normal =
        vec4(M * vec4(vertNor, 0.0f)).xyz;
    vertex_tex = vertTex;
    sun_pos = sunPosition;
    gl_Position = P * V * tpos;
}

