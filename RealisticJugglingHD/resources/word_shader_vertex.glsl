#version 330 core
uniform mat4 P;
uniform mat4 V;
uniform mat4 M;

layout(location = 0) in vec3 vertPos;
layout(location = 1) in vec3 vertColor;

out vec3 vertex_pos;
out vec3 vertex_color;

void main() {
    vec4 tpos =  M * vec4(vertPos, 1.0);
    vertex_pos = tpos.xyz;
    gl_Position = P * V * tpos;

	vertex_color = vertColor;
}
