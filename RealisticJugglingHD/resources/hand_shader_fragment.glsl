#version 330 core
uniform sampler2D handTex;

in vec3 vertex_pos;
in vec3 vertex_normal;
in vec2 vertex_tex;

out vec4 color;

void main() {
    color = vec4(0, 1, 1, 1);
}

