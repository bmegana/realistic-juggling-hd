#version 330 core
uniform sampler2D ballTex;
uniform sampler2D ballSpecTex;
uniform sampler2D ballTex2;

in vec3 vertex_pos;
in vec3 vertex_normal;
in vec2 vertex_tex;

in vec3 sun_pos;

out vec4 color;

void main() {
    vec3 normal = normalize(vertex_normal);
    vec3 lightPos = sun_pos;
    vec3 lightDir =
        normalize(vertex_pos - lightPos);
    float diffuse = dot(normal, lightDir);

    vec4 tcol = texture(ballTex, vertex_tex);
    vec4 tcol2 = texture(ballTex2, vertex_tex);
    color = (tcol + tcol2) * diffuse * 0.7;

    vec3 camDir = 
        normalize(vertex_pos - vec3(0, 0, 0));
    vec3 h = normalize(camDir + lightDir);

    float spec = dot(normal, h);
    spec = clamp(spec, 0, 1);
    spec = pow(spec, 20);

    vec4 specFactor = texture(ballSpecTex, vertex_tex);
    color += specFactor * spec;
}

