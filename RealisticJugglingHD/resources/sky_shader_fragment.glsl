#version 330 core
uniform sampler2D skyTex;
uniform sampler2D skySpecTex;

in vec3 vertex_pos;
in vec3 vertex_normal;
in vec2 vertex_tex;

in vec3 sun_pos;

out vec4 color;

void main()
{
    vec3 normal = normalize(vertex_normal);
    vec3 lightPos = vec3(0, 0, 0);
    vec3 lightDir =
        normalize(vertex_pos - lightPos);
    float diffuse = dot(normal, lightDir);

    vec4 tcol = texture(skyTex, vertex_tex);
    vec3 sunPos = sun_pos;
    float sunHeight = sunPos.y;
    color = tcol * diffuse;
    sunHeight = clamp(sunHeight, 0.02, 1);

    color.r *= sunHeight * 4;
    if (sunHeight > 0.02f)
    {
        color.g *= pow(sunHeight * 4, 2.0f);
        color.b *= pow(sunHeight * 4, 2.0f);
    }
    else
    {
        color.g *= sunHeight * 4;
        color.b *= sunHeight * 4;
    }

    vec3 sunDir =
        normalize(vertex_pos - sunPos);
    vec3 h = normalize(sunDir + lightDir);

    float spec = dot(normal, h);
    spec = clamp(spec, 0, 1);
    spec = pow(spec, 20);
    vec4 specFactor = texture(skySpecTex, vertex_tex);

    specFactor.r *= spec;
    specFactor.g *= spec / 1.75f;
    specFactor.b *= spec / 2.25f;
    color += specFactor;
}
