#include "Rigidbody.h"

Rigidbody::Rigidbody(Transform *t, float m)
{
    transform = t;
    mass = m;
}

void Rigidbody::init()
{
    force.x = 0;
    force.y = 0;
    force.z = 0;
}

void Rigidbody::applyForce(vec3 f)
{
    force += f;
}

void Rigidbody::removeForce()
{
    force = vec3(0, 0, 0);
}

void Rigidbody::setVelocity(vec3 v)
{
    velocity = v;
}

void Rigidbody::setPosToPrev()
{
    transform->setPosition(prevPosition);
}

float Rigidbody::getMass()
{
    return mass;
}

vec3 Rigidbody::getVelocity()
{
    return velocity;
}

void Rigidbody::simulate(float dt)
{
    velocity += (force / mass) * dt;
    prevPosition = transform->getPosition();
    
    vec3 newPos = prevPosition + velocity * dt;
    transform->setPosition(newPos);
}

