#include "Collider.h"

Collider::Collider(Transform *t)
{
    transform = t;
}

vec3 Collider::getPosition()
{
    return transform->getPosition();
}

vec3 Collider::getSize()
{
    return transform->getScale();
}

