#ifndef BOX_COLLIDER_H
#define BOX_COLLIDER_H

#include "Collider.h"

class BoxCollider : public Collider
{
public:
    BoxCollider(Transform *t);
    bool checkCollision(Collider *other);
};
#endif


