#include "SphereCollider.h"

SphereCollider::SphereCollider(Transform *t) : Collider(t)
{
}

bool SphereCollider::checkCollision(Collider *other)
{
    vec3 position = getPosition();
    float radius = getSize().x;

    vec3 diffVec = abs(other->getPosition() - position);
    float diffLength = length(diffVec);
    float radSum = radius + (other->getSize().x);

    return diffLength < radSum;
}

