#include "BoxCollider.h"

BoxCollider::BoxCollider(Transform *t) : Collider(t)
{
}

bool BoxCollider::checkCollision(Collider *other)
{
    vec3 position = getPosition();
    vec3 size = getSize();

    bool isColX =
        position.x + abs(size.x) >= other->getPosition().x &&
        other->getPosition().x + abs(other->getSize().x) >= position.x;
    bool isColY =
        position.y + abs(size.y) >= other->getPosition().y &&
        other->getPosition().y + abs(other->getSize().y) >= position.y;
    bool isColZ =
        position.z + abs(size.z) >= other->getPosition().z &&
        other->getPosition().z + abs(other->getSize().z) >= position.z;

    return isColX && isColY && isColZ;
}

