#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace glm;

class Transform
{
    vec3 initPosition;
    vec3 position;
    vec3 scale;

public:
    Transform(vec3 pos, vec3 size);

    vec3 getInitPosition();
    void resetPosition();

    vec3 getPosition();
    void setPosition(vec3 pos);

    vec3 getScale();
    void setScale(vec3 size);
};
#endif

