#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "Transform.h"
#include "Rigidbody.h"
#include "Collider.h"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace glm;

class GameObject
{
    Transform *transform;
    Rigidbody *rigidbody;
    Collider *collider;

public:
    GameObject(Transform *t, Rigidbody *r, Collider *c);
    Transform* getTransform();
    Rigidbody* getRigidbody();
    Collider* getCollider();
};
#endif

