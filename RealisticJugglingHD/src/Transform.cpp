#include "Transform.h"

Transform::Transform(vec3 pos, vec3 size)
{
    initPosition = pos;
    position = pos;
    scale = size;
}

vec3 Transform::getInitPosition()
{
    return initPosition;
}

void Transform::resetPosition()
{
    position = initPosition;
}

vec3 Transform::getPosition()
{
    return position;
}

void Transform::setPosition(vec3 pos)
{
    position = pos;
}

vec3 Transform::getScale()
{
    return scale;
}

void Transform::setScale(vec3 size)
{
    scale = size;
}

