#ifndef RIGIDBODY_H
#define RIGIDBODY_H

#include "Transform.h"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace glm;

class Rigidbody
{
    float mass;
    Transform *transform;
    vec3 prevPosition;
    vec3 velocity;
    vec3 force;

public:
    Rigidbody(Transform *t, float m);
    void init();

    void applyForce(vec3 f);
    void removeForce();

    void setPosToPrev();

    float getMass();

    void setVelocity(vec3 v);
    vec3 getVelocity();

    void simulate(float dt);
};
#endif

