#ifndef COLLIDER_H
#define COLLIDER_H

#include <cmath>
#include "Transform.h"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace glm;

class Collider
{
    Transform *transform;

protected:
    Collider(Transform *t);
public:
    vec3 getPosition();
    vec3 getSize();
    virtual bool checkCollision(Collider *other) {return false;}
};
#endif

