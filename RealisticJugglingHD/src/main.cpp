#include <iostream>
#include <glad/glad.h>
#include <math.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "GLSL.h"
#include "Program.h"
#include "MatrixStack.h"
#include "Shape.h"

#include "GameObject.h"
#include "SphereCollider.h"
#include "BoxCollider.h"

#include "WindowManager.h"

using namespace std;
using namespace glm;

class Application : public EventCallbacks
{
public:
    WindowManager *windowManager = nullptr;

    void init(const string& resourceDirectory)
    {
        GLSL::checkVersion();

        glEnable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        initSkyShader(resourceDirectory);
        initHandShader(resourceDirectory);
        loadHandMesh(resourceDirectory);
        initWordShader(resourceDirectory);

        vec3 leftHandSize = vec3(-0.25f, 0.25f, 0.25f);
        vec3 initLeftHandPos = vec3(-1.5f, -0.75f, -2.5f);
        Transform *leftHandTransform = new Transform(initLeftHandPos, leftHandSize);
        leftHand = new GameObject(
                leftHandTransform,
                new Rigidbody(leftHandTransform, 1.0f),
                new BoxCollider(leftHandTransform)
                );

        vec3 rightHandSize = vec3(0.25f, 0.25f, 0.25f);
        vec3 initRightHandPos = vec3(1.5f, -0.75f, -2.5f);
        Transform *rightHandTransform = new Transform(initRightHandPos, rightHandSize);
        rightHand = new GameObject(
                rightHandTransform,
                new Rigidbody(rightHandTransform, 1.0f),
                new BoxCollider(rightHandTransform)
                );

        initBallShader(resourceDirectory);
        loadSphereMesh(resourceDirectory);

        balls = new vector<GameObject*>();
        vec3 ballSize = vec3(0.1f, 0.1f, 0.1f);

        bool leftSide = true;
        for (int i = 0; i < numBalls; i++)
        {
            vec3 initBallPos;
            if (i == 0)
            {
                initBallPos = vec3(-1.5f, 0.5f, -2.5f);
            }
            else if (i == 1)
            {
                initBallPos = vec3(1.5f, 0.5f, -2.5f);
            }
            else if (leftSide)
            {
                initBallPos = vec3(-1.5f, 3.0f, -2.5f);
                leftSide = false;
            }
            else
            {
                initBallPos = vec3(1.5f, 6.0f, -2.5f);
                leftSide = true;
            }
            Transform *ballTransform = new Transform(initBallPos, ballSize);
            GameObject *ball = new GameObject(
                    ballTransform,
                    new Rigidbody(ballTransform, 1.0f),
                    new SphereCollider(ballTransform)
                    );
            balls->push_back(ball);
        }
    }

    void initGeom()
    {
        generateSkyTexture();
        generateHandTexture();
        generateBallTexture();
        generateWordVertexBuffer();
        glBindVertexArray(0);
    }

    void render()
    {
        int width, height;
        glfwGetFramebufferSize(windowManager->getHandle(), &width, &height);
        glViewport(0, 0, width, height);

        // Clear framebuffer.
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        mat4 V, P;
        V = mat4(1);

        P = perspective(
                (float)(3.14159 / 4.),
                (float)((float)width / (float)height),
                0.1f,
                1000.0f
                );

        renderSky(P, V);

        for (int i = 0; i < numBalls; i++)
        {
            calculateBallPhysics(balls->at(i));
            renderBall(P, V, balls->at(i));
        }

        mat4 M = animateLeftHand();
        renderHand(P, V, M);
        M = animateRightHand();
        renderHand(P, V, M);

        if (gameTime >= 1.0f)
        {
            gameTime = 1.0f;
            renderWord(P, V);
        }
    }

    void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods)
    {
        if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        {
            glfwSetWindowShouldClose(window, GL_TRUE);
        }

        if (key == GLFW_KEY_A && action == GLFW_PRESS)
        {
            keyAPressed = true;
        }
        if (key == GLFW_KEY_A && action == GLFW_RELEASE)
        {
            keyAPressed = false;
        }

        if (key == GLFW_KEY_S && action == GLFW_PRESS)
        {
            keySPressed = true;
        }
        if (key == GLFW_KEY_S && action == GLFW_RELEASE)
        {
            keySPressed = false;
            keySReleased = true;
        }

        if (key == GLFW_KEY_L && action == GLFW_PRESS)
        {
            keyLPressed = true;
        }
        if (key == GLFW_KEY_L && action == GLFW_RELEASE)
        {
            keyLPressed = false;
        }

        if (key == GLFW_KEY_K && action == GLFW_PRESS)
        {
            keyKPressed = true;
        }
        if (key == GLFW_KEY_K && action == GLFW_RELEASE)
        {
            keyKPressed = false;
            keyKReleased = true;
        }

        if (key == GLFW_KEY_R && action == GLFW_PRESS)
        {
            for (int i = 0; i < numBalls; i++)
            {
                balls->at(i)->getTransform()->resetPosition();
                balls->at(i)->getRigidbody()->removeForce();
                balls->at(i)->getRigidbody()->setVelocity(vec3(0, 0, 0));
            }
            gameOver = false;
            gameStart = false;
            gameTime = -1.0f;
			scaleValue = 0.0f;

            ballOccupyingLeft = nullptr;
            ballOccupyingRight = nullptr;
        }
        if (key == GLFW_KEY_R && action == GLFW_RELEASE)
        {
        }
    }

    void mouseCallback(GLFWwindow *window, int button, int action, int mods)
    {
    }

    void resizeCallback(GLFWwindow *window, int in_width, int in_height)
    {
        //get the window size - may be different then pixels for retina
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);
        glViewport(0, 0, width, height);
    }

private:
    const float RAD = 180.0f / 3.14159265358f;

    shared_ptr<Program> skyShaderProg;
    GLuint skyTexture;
    GLuint skySpecTexture;
    float skyAngleY = 0.0f;
    float sunPosX = 0.0f;
    float sunPosY = 0.225f;
    float sunPosZ = -0.7f;

    shared_ptr<Program> handShaderProg;
    shared_ptr<Shape> hand;
    GLuint handTexture;
    float maxThrowDist = 0.5f;
    float throwSpeed = 0.025f;

    GameObject *leftHand = nullptr;
    GameObject *ballOccupyingLeft = nullptr;
    float currLeftThrowVal = 0.0f;
    bool leftHandRetracting = false;
    bool isThrowingFromLeft = false;
    //used to prevent a ball from being thrown while the left hand is retracting
    bool leftHasThrown = false;

    GameObject *rightHand = nullptr;
    GameObject *ballOccupyingRight = nullptr;
    float currRightThrowVal = 0.0f;
    bool rightHandRetracting = false;
    bool isThrowingFromRight = false;
    //used to prevent a ball from being thrown while the right hand is retracting
    bool rightHasThrown = false;

    shared_ptr<Program> ballShaderProg;
    shared_ptr<Shape> sphere;
    GLuint ballTexture;
    GLuint ballSpecTexture;
    GLuint ballTexture2;
    const int numBalls = 4;
    vector<GameObject*> *balls;

    shared_ptr<Program> wordShaderProg;
    GLuint wordVAO;
    GLuint wordVertexVBO;
    GLuint wordColorVBO;
    GLuint wordIndexVBO;
	float scaleValue = 0;

    bool keyAPressed;
    bool keySPressed;
    bool keySReleased;
    bool keyLPressed;
    bool keyKPressed;
    bool keyKReleased;

    float gameTime = -1.0f;
    bool gameStart = false;
    bool gameOver = false;

    /*
     * Shader and Mesh Initialization Functions
     */
    void initSkyShader(const string& resourceDirectory)
    {
        skyShaderProg = make_shared<Program>();
        skyShaderProg->setVerbose(true);
        skyShaderProg->setShaderNames(
                resourceDirectory + "/sky_shader_vertex.glsl",
                resourceDirectory + "/sky_shader_fragment.glsl"
                );
        skyShaderProg->init();
        skyShaderProg->addUniform("P");
        skyShaderProg->addUniform("V");
        skyShaderProg->addUniform("M");
        skyShaderProg->addUniform("sunPosition");

        skyShaderProg->addAttribute("vertPos");
        skyShaderProg->addAttribute("vertNor");
        skyShaderProg->addAttribute("vertTex");
    }

    void initHandShader(const string& resourceDirectory)
    {
        handShaderProg = make_shared<Program>();
        handShaderProg->setVerbose(true);
        handShaderProg->setShaderNames(
                resourceDirectory + "/hand_shader_vertex.glsl",
                resourceDirectory + "/hand_shader_fragment.glsl"
                );
        handShaderProg->init();
        handShaderProg->addUniform("P");
        handShaderProg->addUniform("V");
        handShaderProg->addUniform("M");

        handShaderProg->addAttribute("vertPos");
        handShaderProg->addAttribute("vertNor");
        handShaderProg->addAttribute("vertTex");
    }

    void loadHandMesh(const string& resourceDirectory)
    {
        hand = make_shared<Shape>();
        hand->loadMesh(resourceDirectory + "/hand.obj");
        hand->resize();
        hand->init();
    }

    void initBallShader(const string& resourceDirectory)
    {
        ballShaderProg = make_shared<Program>();
        ballShaderProg->setVerbose(true);
        ballShaderProg->setShaderNames(
                resourceDirectory + "/sphere_shader_vertex.glsl",
                resourceDirectory + "/sphere_shader_fragment.glsl"
                );
        ballShaderProg->init();
        ballShaderProg->addUniform("P");
        ballShaderProg->addUniform("V");
        ballShaderProg->addUniform("M");
        ballShaderProg->addUniform("sunPosition");

        ballShaderProg->addAttribute("vertPos");
        ballShaderProg->addAttribute("vertNor");
        ballShaderProg->addAttribute("vertTex");
    }

    void loadSphereMesh(const string& resourceDirectory)
    {
        sphere = make_shared<Shape>();
        sphere->loadMesh(resourceDirectory + "/sphere.obj");
        sphere->resize();
        sphere->init();
    }

    void initWordShader(const string& resourceDirectory)
    {
        wordShaderProg = make_shared<Program>();
        wordShaderProg->setVerbose(true);
        wordShaderProg->setShaderNames(
                resourceDirectory + "/word_shader_vertex.glsl",
                resourceDirectory + "/word_shader_fragment.glsl"
                );
        wordShaderProg->init();
        wordShaderProg->addUniform("P");
        wordShaderProg->addUniform("V");
        wordShaderProg->addUniform("M");

        wordShaderProg->addAttribute("vertPos");
        wordShaderProg->addAttribute("vertColor");
    }

    /*
     * Texture and Vertex Buffer Generation Functions
     */
    void generateSkyTexture()
    {
        glBindVertexArray(0);

        int width, height, channels;
        char filepath[1000];

        string str = "../resources/sky.jpg";
        strcpy(filepath, str.c_str());
        unsigned char* data = stbi_load(filepath, &width, &height, &channels, 4);
        glGenTextures(1, &skyTexture);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, skyTexture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        str = "../resources/sky_specular.jpg";
        strcpy(filepath, str.c_str());
        data = stbi_load(filepath, &width, &height, &channels, 4);
        glGenTextures(1, &skySpecTexture);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, skySpecTexture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        GLuint skyTexLoc = glGetUniformLocation(skyShaderProg->pid, "skyTex");
        GLuint skySpecTexLoc = glGetUniformLocation(skyShaderProg->pid, "skySpecTex");

        glUseProgram(skyShaderProg->pid);
        glUniform1i(skyTexLoc, 0);
        glUniform1i(skySpecTexLoc, 1);
    }

    void generateHandTexture()
    {
        glBindVertexArray(0);

        int width, height, channels;
        char filepath[1000];

        string str = "../resources/hand_texture.jpg";
        strcpy(filepath, str.c_str());
        unsigned char* data = stbi_load(filepath, &width, &height, &channels, 4);
        glGenTextures(1, &handTexture);
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, handTexture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        GLuint handTexLoc = glGetUniformLocation(handShaderProg->pid, "handTex");
        
        glUseProgram(handShaderProg->pid);
        glUniform1i(handTexLoc, 2);
    }

    void generateBallTexture()
    {
        glBindVertexArray(0);

        int width, height, channels;
        char filepath[1000];

        string str = "../resources/earth_map.jpg";
        strcpy(filepath, str.c_str());
        unsigned char* data = stbi_load(filepath, &width, &height, &channels, 4);

        glGenTextures(1, &ballTexture);
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, ballTexture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        str = "../resources/earth_spec.jpg";
        strcpy(filepath, str.c_str());
        data = stbi_load(filepath, &width, &height, &channels, 4);

        glGenTextures(1, &ballSpecTexture);
        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_2D, ballSpecTexture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        str = "../resources/earth_clouds.jpg";
        strcpy(filepath, str.c_str());
        data = stbi_load(filepath, &width, &height, &channels, 4);

        glGenTextures(1, &ballTexture2);
        glActiveTexture(GL_TEXTURE5);
        glBindTexture(GL_TEXTURE_2D, ballTexture2);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        GLuint ballTexLoc = glGetUniformLocation(ballShaderProg->pid, "ballTex");
        GLuint ballSpecTexLoc = glGetUniformLocation(ballShaderProg->pid, "ballSpecTex");
        GLuint ballTex2Loc = glGetUniformLocation(ballShaderProg->pid, "ballTex2");

        glUseProgram(ballShaderProg->pid);
        glUniform1i(ballTexLoc, 3);
        glUniform1i(ballSpecTexLoc, 4);
        glUniform1i(ballTex2Loc, 5);
    }

    void generateWordVertexBuffer()
    {
        glGenVertexArrays(1, &wordVAO);
        glBindVertexArray(wordVAO);

        glGenBuffers(1, &wordVertexVBO);
        glBindBuffer(GL_ARRAY_BUFFER, wordVertexVBO);
        GLfloat cube_vertices[] = {
            // front
            -1.0, -1.0,  1.0,
            1.0, -1.0,  1.0,
            1.0,  1.0,  1.0,
            -1.0,  1.0,  1.0,
            // back
            -1.0, -1.0, -1.0,
            1.0, -1.0, -1.0,
            1.0,  1.0, -1.0,
            -1.0,  1.0, -1.0,
        };
        for (int i = 0; i < 24; i++)
            cube_vertices[i] *= 0.5;
        glBufferData(GL_ARRAY_BUFFER, sizeof(cube_vertices), cube_vertices, GL_DYNAMIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

        GLfloat cube_colors[] = {
            // front colors
            1.0, 0.0, 0.0,
            0.0, 1.0, 0.0,
            0.0, 0.0, 1.0,
            1.0, 1.0, 1.0,
            // back colors
            1.0, 0.0, 0.0,
            0.0, 1.0, 0.0,
            0.0, 0.0, 1.0,
            1.0, 1.0, 1.0,
        };
        glGenBuffers(1, &wordColorVBO);
        glBindBuffer(GL_ARRAY_BUFFER, wordColorVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(cube_colors), cube_colors, GL_STATIC_DRAW);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

        GLushort cube_elements[] = {
            // front
            0, 1, 2,
            2, 3, 0,
            // top
            1, 5, 6,
            6, 2, 1,
            // back
            7, 6, 5,
            5, 4, 7,
            // bottom
            4, 0, 3,
            3, 7, 4,
            // left
            4, 5, 1,
            1, 0, 4,
            // right
            3, 2, 6,
            6, 7, 3,
        };
        glGenBuffers(1, &wordIndexVBO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, wordIndexVBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cube_elements), cube_elements, GL_STATIC_DRAW);
        
        glBindVertexArray(0);
    }

    /*
     * Physics and Collision Calculation Functions
     */
    bool leftCanHold(GameObject *ball)
    {
        Collider *ballCol = ball->getCollider();
        Collider *leftHandCollider = leftHand->getCollider();
        return (ballOccupyingLeft == NULL || ball == ballOccupyingLeft) &&
            leftHandCollider->checkCollision(ballCol) &&
            keyAPressed &&
            !isThrowingFromLeft;
    }

    bool rightCanHold(GameObject *ball)
    {
        Collider *ballCol = ball->getCollider();
        Collider *rightHandCollider = rightHand->getCollider();
        return (ballOccupyingRight == NULL || ball == ballOccupyingRight) &&
            rightHandCollider->checkCollision(ballCol) &&
            keyLPressed &&
            !isThrowingFromRight;
    }

    GameObject *getOtherBallCollided(GameObject *ball)
    {
        Collider *ballCol = ball->getCollider();
        for (int i = 0; i < numBalls; i++)
        {
            /*
             * This conditional checks if the ball is comparing against itself.
             * The ball should not compare its collider with itself. Otherwise,
             * the |checkCollision| function would be true, and the ball would
             * not move.
             */
            GameObject *otherBall = balls->at(i);
            Collider *otherBallCol = otherBall->getCollider();
            if (ballCol != otherBallCol)
            {
                if (ball->getCollider()->checkCollision(otherBallCol))
                {
                    return otherBall;
                }
            }
        }
        return NULL;
    }

    void resolveCollision(GameObject *ball, GameObject *otherBall)
    {
        Transform *ballTransform = ball->getTransform();
        Rigidbody *ballRb = ball->getRigidbody();

        Transform *otherBallTransform = otherBall->getTransform();
        Rigidbody *otherBallRb = otherBall->getRigidbody();

        vec3 ballPos = ballTransform->getPosition();
        vec3 otherBallPos = otherBallTransform->getPosition();
        vec3 diffVec = otherBallPos - ballPos;
        vec3 normalizedDiff = normalize(diffVec);

        float a1 = 0;
        float a2 = 0;
        vec3 ballVel = ballRb->getVelocity();
        vec3 otherBallVel = otherBallRb->getVelocity();
        float eps = 0.01f;
        if (!(abs(ballVel.x) < eps && abs(ballVel.y) < eps && abs(ballVel.z) < eps) &&
                !(abs(otherBallVel.x) < eps && abs(otherBallVel.y) < eps && abs(otherBallVel.z) < eps))
        {
            a1 = dot(ballRb->getVelocity(), normalizedDiff);
            a2 = dot(otherBallRb->getVelocity(), normalizedDiff);

            float momentum =
                (2.0f * (a1 - a2)) / (ballRb->getMass() + otherBallRb->getMass());

            vec3 newVel1 = ballVel - (momentum * otherBallRb->getMass()) * normalizedDiff;
            vec3 newVel2 = otherBallVel + (momentum * ballRb->getMass()) * normalizedDiff;

            otherBallRb->setVelocity(newVel2);
            ballRb->setVelocity(newVel1);
        }
    }

    void calculateBallPhysics(GameObject *ball)
    {
        Transform *ballTransform = ball->getTransform();
        Rigidbody *ballRb = ball->getRigidbody();
        Collider *ballCol = ball->getCollider();

        Collider *leftHandCollider = leftHand->getCollider();
        bool ballCollidedLeftHand = leftHandCollider->checkCollision(ballCol);
        Collider *rightHandCollider = rightHand->getCollider();
        bool ballCollidedRightHand = rightHandCollider->checkCollision(ballCol);

        ballRb->init();
        if (gameTime >= 1.0f)
        {
            ballRb->setVelocity(vec3(0, 0.25f, 0));
        }
        else if (ballTransform->getPosition().y > 1.0f)
        {
            ballRb->removeForce();
            if (gameStart)
            {
                ballRb->setVelocity(vec3(0, -0.25f, 0));
            }
            else
            {
                ballRb->setVelocity(vec3(0, 0, 0));
            }
        }
        else if (ballTransform->getPosition().y <= -1.0f)
        {
            gameOver = true;
        }
        if (leftCanHold(ball))
        {
            ballOccupyingLeft = ball;
            ballRb->setVelocity(vec3(0, 0, 0));
            ballRb->removeForce();
            ballTransform->setPosition(leftHandCollider->getPosition());
        }
        else if (rightCanHold(ball))
        {
            ballOccupyingRight = ball;
            ballRb->setVelocity(vec3(0, 0, 0));
            ballRb->removeForce();
            ballTransform->setPosition(rightHandCollider->getPosition());
        }
        else if (ballCollidedLeftHand && isThrowingFromLeft && !leftHasThrown)
        {
            ballRb->setVelocity(vec3(2.5f, 4.5f, 0));
            ballOccupyingLeft = nullptr;
            leftHasThrown = true;
            isThrowingFromLeft = false;
            gameStart = true;
        }
        else if (ballCollidedRightHand && isThrowingFromRight && !rightHasThrown)
        {
            ballRb->setVelocity(vec3(-2.5f, 4.5f, 0));
            ballOccupyingRight = nullptr;
            rightHasThrown = true;
            isThrowingFromRight = false;
            gameStart = true;
        }
        else
        {
            GameObject *otherBall = getOtherBallCollided(ball);
            if (otherBall != NULL)
            {
                resolveCollision(ball, otherBall);
            }
            else
            {
                ballRb->applyForce(vec3(0, -9.81f, 0));
            }
        }
        float deltaTime = gameOver ? 0.0f : 0.0075f;
        ballRb->simulate(deltaTime);
    }

    /*
     * Rendering Functions
     */
    void renderSky(mat4& P, mat4& V)
    {
        float skyAngleX = 3.1415926 / 2.;
        if (gameStart && !gameOver)
        {
            skyAngleY -= 0.00025f;
        }

        mat4 scaleMat = scale(mat4(1.0f), vec3(0.8f, 0.8f, 0.8f));
        mat4 rotMatX = rotate(mat4(1.0f), skyAngleX, vec3(1.0f, 0.0f, 0.0f));
        mat4 rotMatY = rotate(mat4(1.0f), skyAngleY, vec3(0.0f, 1.0f, 0.0f));
        mat4 transMat = translate(mat4(1.0f), vec3(0.0f, 0.0f, 0.0f));
        mat4 M = transMat * rotMatY * rotMatX * scaleMat;

        if (gameStart && !gameOver)
        {
            gameTime += 0.00075f;
            sunPosY = 0.5f * cos(gameTime) - 0.275f;
        }
        sunPosX = -0.5f * sin(gameTime);
        sunPosZ = -0.675f + 0.025 * cos(gameTime);
        vec3 sunPosition = vec3(sunPosX, sunPosY, sunPosZ);

        skyShaderProg->bind();

        glUniformMatrix4fv(skyShaderProg->getUniform("P"), 1, GL_FALSE, &P[0][0]);
        glUniformMatrix4fv(skyShaderProg->getUniform("V"), 1, GL_FALSE, &V[0][0]);
        glUniformMatrix4fv(skyShaderProg->getUniform("M"), 1, GL_FALSE, &M[0][0]);
        glUniform3fv(skyShaderProg->getUniform("sunPosition"), 1, &sunPosition[0]);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, skyTexture);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, skySpecTexture);
        glDisable(GL_DEPTH_TEST);
        sphere->draw(skyShaderProg, false);
        glEnable(GL_DEPTH_TEST);

        skyShaderProg->unbind();
    }

    void renderHand(mat4& P, mat4& V, mat4& M)
    {
        handShaderProg->bind();

        glUniformMatrix4fv(handShaderProg->getUniform("P"), 1, GL_FALSE, &P[0][0]);
        glUniformMatrix4fv(handShaderProg->getUniform("V"), 1, GL_FALSE, &V[0][0]);
        glUniformMatrix4fv(handShaderProg->getUniform("M"), 1, GL_FALSE, &M[0][0]);

        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, handTexture);
        hand->draw(handShaderProg, false);

        glBindVertexArray(0);

        handShaderProg->unbind();
    }

    void renderBall(mat4& P, mat4& V, GameObject *ball)
    {
        float ballAngleX = 3.1415926 / 2.;
        static float ballAngleY = 0;
        if (abs(ball->getRigidbody()->getVelocity().y) > 0.1f)
        {
            ballAngleY += 0.01f;
        }

        vec3 ballScale = ball->getTransform()->getScale();
        ballScale.x = ballScale.x * -1.0f;
        mat4 scaleMat = scale(mat4(1.0f), ballScale);
        mat4 rotMatX = rotate(mat4(1.0f), ballAngleX, vec3(1, 0, 0));
        mat4 rotMatY = rotate(mat4(1.0f), ballAngleY, vec3(0, 1, 0));
        mat4 tranMat = translate(mat4(1.0f), ball->getTransform()->getPosition());
        mat4 M = tranMat * rotMatY * rotMatX * scaleMat;

        vec3 sunPosition = vec3(-sunPosX, -sunPosY, sunPosZ - 10.0f);

        ballShaderProg->bind();

        glUniformMatrix4fv(ballShaderProg->getUniform("P"), 1, GL_FALSE, &P[0][0]);
        glUniformMatrix4fv(ballShaderProg->getUniform("V"), 1, GL_FALSE, &V[0][0]);
        glUniformMatrix4fv(ballShaderProg->getUniform("M"), 1, GL_FALSE, &M[0][0]);
        glUniform3fv(ballShaderProg->getUniform("sunPosition"), 1, &sunPosition[0]);

        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, ballTexture);
        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_2D, ballSpecTexture);
        glActiveTexture(GL_TEXTURE5);
        glBindTexture(GL_TEXTURE_2D, ballTexture2);
        sphere->draw(ballShaderProg, false);

        ballShaderProg->unbind();
    }

    void renderLetterA(float scaleValue)
    {
        mat4 scaleMat = scale(mat4(1.0f), vec3(0.5f, 3, 0.5f + scaleValue));
        mat4 rotMatY = rotate(mat4(1.0f), 0.0f, vec3(0, 1, 0));
        mat4 finalTranMat = translate(mat4(1.0f), vec3(-1, 0, -100));

        //matrix transform for right line of 'A'
        mat4 tempTranMat = translate(mat4(1.0f), vec3(-0.5f, 0, 0));
        mat4 rotMatZ = rotate(mat4(1.0f), 0.25f, vec3(0, 0, 1));
        mat4 M = finalTranMat * rotMatY * rotMatZ * tempTranMat * scaleMat;

        glUniformMatrix4fv(wordShaderProg->getUniform("M"), 1, GL_FALSE, &M[0][0]);
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, (void*)0);

        //matrix transform for left line of 'A'
        tempTranMat = translate(mat4(1.0f), vec3(-1.5f, 0, 0));
        rotMatZ = rotate(mat4(1.0f), -0.25f, vec3(0, 0, 1));
        M = finalTranMat * rotMatY * rotMatZ * tempTranMat * scaleMat;

        glUniformMatrix4fv(wordShaderProg->getUniform("M"), 1, GL_FALSE, &M[0][0]);
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, (void*)0);

        //matrix transform for middle line of 'A'
        mat4 newScaleMat = scale(mat4(1.0f), vec3(0.5f, 1.375f, 0.5f + scaleValue));
        rotMatZ = rotate(mat4(1.0f), 3.14f / 2.0f, vec3(0, 0, 1));

        tempTranMat = translate(mat4(1.0f), vec3(-1.25f, 0.05f, 0));
        M = finalTranMat * rotMatY * tempTranMat * rotMatZ * newScaleMat;

        glUniformMatrix4fv(wordShaderProg->getUniform("M"), 1, GL_FALSE, &M[0][0]);
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, (void*)0);
    }

    void renderLetterFirstY(float scaleValue)
    {
        mat4 scaleMat = scale(mat4(1.0f), vec3(0.5f, 2, 0.5f + scaleValue));
        mat4 rotMatY = rotate(mat4(1.0f), 0.0f, vec3(0, 1, 0));
        mat4 finalTranMat = translate(mat4(1.0f), vec3(1.25f, 0, -100));

        //matrix transform for right line of 'Y'
        mat4 tempTranMat = translate(mat4(1.0f), vec3(-1.0f, 0.5f, 0));
        mat4 rotMatZ = rotate(mat4(1.0f), -0.25f, vec3(0, 0, 1));
        mat4 M = finalTranMat * rotMatY * rotMatZ * tempTranMat * scaleMat;

        glUniformMatrix4fv(wordShaderProg->getUniform("M"), 1, GL_FALSE, &M[0][0]);
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, (void*)0);

        //matrix transform for left line of 'Y'
        tempTranMat = translate(mat4(1.0f), vec3(-1.375f, 1.0f, 0));
        rotMatZ = rotate(mat4(1.0f), 0.25f, vec3(0, 0, 1));
        M = finalTranMat * rotMatY * rotMatZ * tempTranMat * scaleMat;

        glUniformMatrix4fv(wordShaderProg->getUniform("M"), 1, GL_FALSE, &M[0][0]);
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, (void*)0);

        //matrix transform for middle line of 'Y'
        mat4 newScaleMat = scale(mat4(1.0f), vec3(0.5f, 1.375f, 0.5f + scaleValue));
        rotMatZ = rotate(mat4(1.0f), 0.0f, vec3(0, 0, 1));

        tempTranMat = translate(mat4(1.0f), vec3(-1.25f, -0.75f, 0));
        M = finalTranMat * rotMatY * tempTranMat * rotMatZ * newScaleMat;

        glUniformMatrix4fv(wordShaderProg->getUniform("M"), 1, GL_FALSE, &M[0][0]);
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, (void*)0);
    }

    void renderLetterSecondY(float scaleValue)
    {
        mat4 scaleMat = scale(mat4(1.0f), vec3(0.5f, 2, 0.5f + scaleValue));
        mat4 rotMatY = rotate(mat4(1.0f), 0.0f, vec3(0, 1, 0));
        mat4 finalTranMat = translate(mat4(1.0f), vec3(3.0f, 0, -100));

        //matrix transform for right line of 'Y'
        mat4 tempTranMat = translate(mat4(1.0f), vec3(-1.0f, 0.5f, 0));
        mat4 rotMatZ = rotate(mat4(1.0f), -0.25f, vec3(0, 0, 1));
        mat4 M = finalTranMat * rotMatY * rotMatZ * tempTranMat * scaleMat;

        glUniformMatrix4fv(wordShaderProg->getUniform("M"), 1, GL_FALSE, &M[0][0]);
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, (void*)0);

        //matrix transform for left line of 'Y'
        tempTranMat = translate(mat4(1.0f), vec3(-1.375f, 1.0f, 0));
        rotMatZ = rotate(mat4(1.0f), 0.25f, vec3(0, 0, 1));
        M = finalTranMat * rotMatY * rotMatZ * tempTranMat * scaleMat;

        glUniformMatrix4fv(wordShaderProg->getUniform("M"), 1, GL_FALSE, &M[0][0]);
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, (void*)0);

        //matrix transform for middle line of 'Y'
        mat4 newScaleMat = scale(mat4(1.0f), vec3(0.5f, 1.375f, 0.5f + scaleValue));
        rotMatZ = rotate(mat4(1.0f), 0.0f, vec3(0, 0, 1));

        tempTranMat = translate(mat4(1.0f), vec3(-1.25f, -0.75f, 0));
        M = finalTranMat * rotMatY * tempTranMat * rotMatZ * newScaleMat;

        glUniformMatrix4fv(wordShaderProg->getUniform("M"), 1, GL_FALSE, &M[0][0]);
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, (void*)0);
    }

    void renderWord(mat4& P, mat4& V)
    {
        wordShaderProg->bind();
        glBindVertexArray(wordVAO);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, wordIndexVBO);

        glUniformMatrix4fv(wordShaderProg->getUniform("P"), 1, GL_FALSE, &P[0][0]);
        glUniformMatrix4fv(wordShaderProg->getUniform("V"), 1, GL_FALSE, &V[0][0]);

        scaleValue += 1.25f;
        renderLetterA(scaleValue);
        renderLetterFirstY(scaleValue);
        renderLetterSecondY(scaleValue);

        glBindVertexArray(0);
        wordShaderProg->unbind();
    }

    /*
     * Animation Functions
     */
    mat4 animateLeftHand()
    {
        if (currLeftThrowVal >= maxThrowDist || keySReleased)
        {
            leftHandRetracting = true;
            keySReleased = false;
        }

        if (leftHandRetracting && currLeftThrowVal > 0.0f)
        {
            isThrowingFromLeft = true;
            currLeftThrowVal -= throwSpeed;
        }
        else if (keyAPressed && keySPressed && !leftHandRetracting)
        {
            currLeftThrowVal += throwSpeed;
        }
        else
        {
            leftHandRetracting = false;
            isThrowingFromLeft = false;
            currLeftThrowVal = 0.0f;
            leftHasThrown = false;
        }
        Transform *leftHandTransform = leftHand->getTransform();
        leftHandTransform->setPosition(
                leftHandTransform->getInitPosition() +
                vec3(currLeftThrowVal, currLeftThrowVal, 0)
                );

        mat4 scaleMat = scale(mat4(1.0f), leftHandTransform->getScale());
        mat4 rotZMat = rotate(mat4(1.0f), RAD * 2.25f, vec3(0, 0, 1));
        mat4 rotYMat = rotate(mat4(1.0f), RAD * 1.85f, vec3(0, 1, 0));
        mat4 tranMat = translate(mat4(1.0f), leftHandTransform->getPosition());
        mat4 M = tranMat * rotYMat * rotZMat * scaleMat;

        return M;
    }

    mat4 animateRightHand()
    {
        if (currRightThrowVal >= maxThrowDist || keyKReleased)
        {
            rightHandRetracting = true;
            keyKReleased = false;
        }

        if (rightHandRetracting && currRightThrowVal > 0.0f)
        {
            isThrowingFromRight = true;
            currRightThrowVal -= throwSpeed;
        }
        else if (keyLPressed && keyKPressed && !rightHandRetracting)
        {
            currRightThrowVal += throwSpeed;
        }
        else
        {
            rightHandRetracting = false;
            isThrowingFromRight = false;
            currRightThrowVal = 0.0f;
            rightHasThrown = false;
        }
        Transform *rightHandTransform = rightHand->getTransform();
        rightHandTransform->setPosition(
                rightHandTransform->getInitPosition() +
                vec3(-currRightThrowVal, currRightThrowVal, 0)
                );

        mat4 scaleMat = scale(mat4(1.0f), rightHandTransform->getScale());
        mat4 rotZMat = rotate(mat4(1.0f), RAD * 2.25f, vec3(0, 0, 1));
        mat4 rotYMat = rotate(mat4(1.0f), RAD * 3.965f, vec3(0, 1, 0));
        mat4 tranMat = translate(mat4(1.0f), rightHandTransform->getPosition());
        mat4 M = tranMat * rotYMat * rotZMat * scaleMat;

        return M;
    }
};

int main(int argc, char **argv)
{
    string resourceDir = "../resources";
    if (argc >= 2)
    {
        resourceDir = argv[1];
    }

    Application *application = new Application();

    WindowManager * windowManager = new WindowManager();
    windowManager->init(1920, 1080);
    windowManager->setEventCallbacks(application);
    application->windowManager = windowManager;

    application->init(resourceDir);
    application->initGeom();

    while(!glfwWindowShouldClose(windowManager->getHandle()))
    {
        application->render();
        glfwSwapBuffers(windowManager->getHandle());
        glfwPollEvents();
    }

    windowManager->shutdown();
    return 0;
}

