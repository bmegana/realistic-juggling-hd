#include "GameObject.h"

GameObject::GameObject(Transform *t, Rigidbody *r, Collider *c)
{
    transform = t;
    rigidbody = r;
    collider = c;
}

Transform* GameObject::getTransform()
{
    return transform;
}

Rigidbody* GameObject::getRigidbody()
{
    return rigidbody;
}

Collider* GameObject::getCollider()
{
    return collider;
}

