#ifndef SPHERE_COLLIDER_H
#define SPHERE_COLLIDER_H

#include "Collider.h"

class SphereCollider : public Collider
{
public:
    SphereCollider(Transform *t);
    bool checkCollision(Collider *other);
};
#endif

